# Sen Bot

Discord bot exclusively for Senzawa's server.
Last updated October 21, 2019.


OVERVIEW
-------------------------------
The SenBot operates by initializing and running commands through "packs". These packs are isolated scripts that are referenced from the packManager.js script.

For discord.js documentation and examples, visit:
https://discord.js.org/#/docs/main/stable/general/welcome
-------------------------------


INSTRUCTIONS
-------------------------------
>Requirements:
-------------------------------
Packs that you create should follow these requirements:

- All of your pack-related scripts and files must be contained in a single isolated folder in the "packs" folder.

- Should include the functions "initialize(client, guild)", and "processCommand(msg,command,params)".

- Commands should make sense and be as easy to understand and remember for members as possible.

- An optional README.txt file can be included in the pack folder to briefly describe the purpose and uses of the pack.

-------------------------------
>Creating a Pack:
-------------------------------
To create a pack, simply create a new folder in the "packs" folder with a relevant name to your pack, i.e a "./packs/birthday" folder for a birthday-related pack.

You can have as many scripts or files as needed for your code, but all files should be references from a single js script with the following functions:

- initialize(client, guild)
This function is called once every time the bot is started up. It retrieves the bot's discord client, and the relevant guild (Senzawa's server).

- processCommand(msg, command, params)
This function is called every time a member sends a message starting with the prefix (-). "msg" is the message data sent from the member, "command" is the first word immediately after the prefix, and "params" are any words following the command, separated by spaces. i.e -bday refresh; where bday is the command, and refresh is the first and only param.

- processMessage(msg)
This function is called every time a user sends a message. The variable msg contains the raw discord.js message object.

- processEdit(oldMsg, newMsg)
This function is called every time a user edits a message. If the bot was running while the original message was sent the oldMsg varibale will contain the original message
object. The newMsg variable will contain the new edited version of the message.

The tools.js script contains helper functions that may be of use in your pack. You may add general helper functions to this script, but please keep it tidy.

When you've finished your pack, add your pack and its initialize and processCommand functions to packManager.js.
-------------------------------
