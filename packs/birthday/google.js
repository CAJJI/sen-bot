const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');


const SCOPES = ['https://www.googleapis.com/auth/forms', 'https://www.googleapis.com/auth/spreadsheets'];

const TOKEN_PATH = './packs/birthday/token.json';

methods = {

callFunction :function(functionName, callback){  
    fs.readFile('./packs/birthday/credentials.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        // Authorize a client with credentials, then call the Google Apps Script API.
        this.authorize(JSON.parse(content), this.callScriptFunction, functionName, callback);        
    });  
},

authorize : function(credentials, callback, functionName, finalCallback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return this.getAccessToken(oAuth2Client, callback, functionName, finalCallback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client, functionName, finalCallback);
  });
},

getAccessToken: function(oAuth2Client, callback, functionName, finalCallback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  //rl.question('Enter the code from that page here: ', (code) => {
    //rl.close();
    var code = "4/qQER6fFhyUUNWrshI9ZXFu0_rr3PXW7qYD9hLRtQ66kz0cRg4ZOmWOo";
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error retrieving access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client, functionName, finalCallback);
    });
  //});
},


callScriptFunction : function(auth, functionName, callback) {    
    var scriptId = "1xefYbe_GpiIbt3thgjojecxm-hxKWNXOy69-XJ5Ukr2DGonqYhSlwdJO";    
      
    // Call the Apps Script API run method
    //   'scriptId' is the URL parameter that states what script to run
    //   'resource' describes the run request body (with the function name
    //              to execute)        

    const script = google.script('v1');

    // Make the API request. The request object is included here as 'resource'.
    script.scripts.run({
      auth: auth,
      resource: {
        function: functionName,
      },
      scriptId: scriptId,
    }, function(err, resp) {
      if (err) {
        // The API encountered a problem before the script started executing.
        console.log('The API returned an error: ' + err);
        return;
      }
      if (resp.error) {
        // The API executed, but the script returned an error.
  
        // Extract the first (and only) set of error details. The values of this
        // object are the script's 'errorMessage' and 'errorType', and an array
        // of stack trace elements.
        const error = resp.error.details[0];
        console.log('Script error message: ' + error.errorMessage);
        console.log('Script error stacktrace:');
  
        if (error.scriptStackTraceElements) {
          // There may not be a stacktrace if the script didn't start executing.
          for (let i = 0; i < error.scriptStackTraceElements.length; i++) {
            const trace = error.scriptStackTraceElements[i];
            console.log('\t%s: %s', trace.function, trace.lineNumber);
          }
        }
      } else {                
        callback(resp.data.response.result);
        return resp.data.response.result;        
      }
    });
  }
}

module.exports = methods