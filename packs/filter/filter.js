const Discord = require("discord.js");

const logChannel = "518593808020013057";

const adminId = '482300197070831627';
const modId = '519297711309193218';
const hmId = '542811191077109771';
const testID = '618825061490491434'; //only used for the test server

const regexes = [
    /(n|ⓝ|⒩|ｎ)+\W*((i|ⓘ|⒤|ｉ|1|①|⑴|⒈)|((e|ⓔ|⒠|ｅ|3)+\W*(e|ⓔ|⒠|ｅ|3)+))+\W*(g|ⓖ|⒢|ｇ)+\W*(g|ⓖ|⒢|ｇ)+\W*(((e|ⓔ|⒠|ｅ|3)+\W*(r|ⓡ|⒭|ｒ)+\W*)|(a|ⓐ|⒜|4|@|ａ|④|⑷)+|(y|ⓨ|⒴|ｙ)+)/gmiu,
    /(r|ⓡ|⒭|ｒ)+\W*(e|ⓔ|⒠|ｅ|3)+\W*(g|ⓖ|⒢|ｇ)+\W*(g|ⓖ|⒢|ｇ)+\W*(i|ⓘ|⒤|ｉ|1|①|⑴|⒈)+\W*(n|ⓝ|⒩|ｎ)+/gmiu,    
    /(c|ⓒ|⒞|ｃ)+\W*(h|ⓗ|⒣|ｈ)+\W*(i|ⓘ|⒤|ｉ|1|①|⑴|⒈)+\W*(n|ⓝ|⒩|ｎ)+\W*(k|ⓚ|⒦|ｋ)+/gmiu,
    /(t|ⓣ|⒯|ｔ)+\W*(r|ⓡ|⒭|ｒ)+\W*(a|ⓐ|⒜|4|@|ａ|④|⑷)+\W*(n|ⓝ|⒩|ｎ)+\W*(n|ⓝ|⒩|ｎ)+\W*(((i|ⓘ|⒤|ｉ|1|①|⑴|⒈)+\W*(e|ⓔ|⒠|ｅ|3)+\W*)|(y|ⓨ|⒴|ｙ))/gmiu,
    /(b|ⓑ|⒝|ｂ)+\W*(e|ⓔ|⒠|ｅ|3)+\W*(a|ⓐ|⒜|4|@|ａ|④|⑷)+\W*(n|ⓝ|⒩|ｎ)+\W*(e|ⓔ|⒠|ｅ|3)+\W*(r|ⓡ|⒭|ｒ)+\W*/gmiu,
    /(b|ⓑ|⒝|ｂ)+\W*(ö)+\W*(g|ⓖ|⒢|ｇ)+\W*/gmiu,
    /(z|ⓩ|⒵|ｚ)+\W*(i|ⓘ|⒤|ｉ|1|①|⑴|⒈)+\W*(p)+\W*(p)+\W*(e|ⓔ|⒠|ｅ|3)+\W*(r|ⓡ|⒭|ｒ)+\W*(h|ⓗ|⒣|ｈ)+\W*(e|ⓔ|⒠|ｅ|3)+\W*(a|ⓐ|⒜|4|@|ａ|④|⑷)+\W*(d|ⓓ|⒟|ｄ)+/gmiu
]

var pack;

methods = {    

    initialize(client, guild){                
        pack = this;
        pack.client = client;
        pack.guild = guild;
        pack.enabled = true;

        console.log("Filter pack initialized.")
    },

    processCommand(msg, command, params){
        if (!msg.member.roles.has(adminId) && !msg.member.roles.has(modId) && !msg.member.roles.has(hmId) && !msg.member.roles.has(testID)) return;
        switch(command){
            case("filter-on"):{
                this.enabled = true;
                msg.reply("Filter Enabled");
                return true;
            }
            case("filter-off"):{
                this.enabled = false;
                msg.reply("Filter Disabled");
                return true;
            }
        }
    },

    processMessage(msg){
        this.filterCheck(msg);
    },

    processEdit(oldMsg, newMsg){
        this.filterCheck(newMsg);
    },

    filterCheck(msg){        
        if (!this.enabled) return;
        if (msg.member == null){
            console.log("null member at filter check: " + msg.contents);
            return;
        }
        if (msg.member.roles.has(adminId) || msg.member.roles.has(modId)|| msg.member.roles.has(hmId)) return;
        
        trimmedMsg = msg.content.replace(/\s*\u200B*/gu, "");
        matched = false;

        for (i in regexes){
            matched = regexes[i].test(trimmedMsg);
            if (matched) break;
        }

        if(matched){
            msg.delete();
            msg.reply("**No bad words please!**");
            this.guild.channels.get(logChannel).send(this.createEmbed(msg))
        }
    },

    createEmbed(msg){
        embed = new Discord.RichEmbed();
        embed.setAuthor("Filter Log");
        embed.setDescription("Message from <@"+msg.author.id+"> deleted in <#"+msg.channel.id+">");
        embed.setColor([255, 33, 225]);
        
        if (msg.content.length > 1024){
            embed.addField("Contents:", msg.content.slice(0,1023));
            embed.addField("\u200B", msg.content.slice(1024,2000));
        }
        else{
            embed.addField("Contents:", msg.content);
        }
        
        return embed;
    }

}

module.exports = methods