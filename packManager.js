//Add pack references here.
const birthday = require('./packs/birthday/birthday.js')
const admin = require('./packs/admin/admin.js')
const battle = require('./packs/battle/battle.js')
const help = require('./packs/help/help.js')
const responses = require('./packs/responses/responses.js')
const filter = require('./packs/filter/filter.js')
const verify = require('./packs/verify/verify.js')

methods = {

    //Add your pack's initialize and processCommand functions below.

    initialize(client, guild){
        birthday.initialize(client, guild);
        admin.initialize(client, guild);
        battle.initialize(client, guild);
        help.initialize(client, guild);
        responses.initialize(client, guild);
        filter.initialize(client, guild);
        verify.initialize(client, guild);
    },

    processCommand(msg, command, params){                
        birthday.processCommand(msg, command, params);
        admin.processCommand(msg, command, params);
        battle.processCommand(msg, command, params);
        help.processCommand(msg, command, params);
        responses.processCommand(msg, command, params);
        filter.processCommand(msg, command, params);
        verify.processCommand(msg, command, params);
    },

    processMessage(msg){
        responses.processMessage(msg);
        filter.processMessage(msg);
    },

    processEdit(oldMsg, newMsg){
        filter.processEdit(oldMsg, newMsg);
    }
}

module.exports = methods;