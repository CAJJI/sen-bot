//on ~help, dm the user a help message pertaining to the existing packs. update this with every pack
const tools = require('./../../tools.js');

var pack;

methods = {

    initialize(client, guild){
        pack = this;
        pack.client = client;
        pack.guild = guild;
    },

    processCommand(msg, command, params){
        switch(command){
            case("help"):{
                this.help(msg);
                return true;
            }
        }
    },

    help(msg){                
        var string = ""
        + "heyo"
        + tools.newLine()
        + "heres the rundown gamer"
        + tools.newLine(2)

        + "**__MODERATION__**"
        + tools.newLine()        
        + "> **~say _[text]_** : Mimics a message (Hall Monitor and up only)."                
        + tools.newLine()
        + "> "
        + tools.newLine()
        + "> *Basic Moderator only commands.*"
        + tools.newLine(2)

        + "**__BIRTHDAY__**"
        + tools.newLine()        
        + "> **~bday** : Get the next upcoming birthday."        
        + tools.newLine()
        + "> **~bday revoke** : Removes all assigned birthday roles (Admin only)."        
        + tools.newLine()
        + "> **~bday refresh** : Refresh and process birthdays (Admin only)."        
        + tools.newLine()
        + "> "
        + tools.newLine()
        + "> *To add your birthday, check the pinned messages in #general for the birthday form and fill it out accurately to automatically receive the birthday role."
        + " Birthdays are automatically refreshed every 24 hours.*"
        + tools.newLine(2)

        + "**__BATTLE__**"
        + tools.newLine()        
        + "> **~slap _[user]_** : Slaps a user."        
        + tools.newLine()        
        + "> **~refresh** : Revives your health to full."        
        + tools.newLine()        
        + "> **~refresh _[user]_** : Revives a user's health to full (Admin only)."        
        + tools.newLine()
        + "> **~score** : Get your wins and losses."        
        + tools.newLine()
        + "> **~score _[user]_** : Get a user's wins and losses."        
        + tools.newLine()
        + "> **~scoreboard** : Get the top ranked users."        
        + tools.newLine()
        + "> **~battle _[user]_** : Simulates a battle with another user."        
        + tools.newLine()
        + "> **~disable** : Disables your ability to engage in battles."        
        + tools.newLine()        
        + "> **~enable** : Enables your ability to engage in battles."        
        + tools.newLine()                
        + "> "
        + tools.newLine()
        + "> *Damage dealt per slap is a random value from 0 to 100. There is a chance to miss or be countered. If you run out of health you are knocked out for 5 minutes, but you can revive"
        + " yourself using the refresh command.*"
        + tools.newLine(2)
        + "pls only use bot commands in the bot channel! and dont abuse or you will be killed by cajji. direct all complaints to brock. uwu"        

        msg.member.send(string);
    }
}

module.exports = methods;