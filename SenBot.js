const Discord = require('discord.js');
const client = new Discord.Client();

const auth = require('./../auth.json')

const tools = require('./tools.js')
const pm = require('./packManager.js')
 
const prefix = "-"

const generalId = "518095108269408283";

const whiteListedCommands =[
    "say",
]

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);      
    console.log(new Date());

    client.user.setActivity('-help');

    var guildId = auth.guildId;

    var guild = client.guilds.get(guildId);    
    
    if (!guild.available)
        console.log("Guild is not available at this time.");

    pm.initialize(client, guild);    
});

client.on('message', msg => {    
    if (!msg.guild) return;
    if (msg.author.bot) return;     

    pm.processMessage(msg);

    if (msg.content.startsWith(prefix)){        
        var text = msg.content.slice(prefix.length);            
        var split = text.split(' ');
        var command = split[0];
        var params = [];

        for (var i = 1; i < split.length; i++){
            params.push(split[i]);
        }        

        if (msg.channel.id === generalId && !whiteListedCommands.includes(command)){
            return;
            //tools.sendMessage(msg, "No bot commands in general please!", false, true);    
            //return;
        }

        pm.processCommand(msg, command, params); 
    }           
});

client.on('messageUpdate', (oldMsg, newMsg) => {
    if (!oldMsg.guild) return;
    if (oldMsg.author.bot) return;     
    pm.processEdit(oldMsg, newMsg);
})


client.login(auth.token);
