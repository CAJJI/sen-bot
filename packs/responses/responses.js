const tools = require('./../../tools.js')

var pack;

const responses =[
    "what",
    "dont talk to me",
    "no u",
    "wow",
    "ok gamer",
    "ill break your arm",
    "im gonna fuck you up now",
    "ok im taking your legs bitch",
    "shut",
    ":dance:",        
    "stop",
    "ok time to die now",
    "SHUT UP",
    "aha",
    "ok",
    ":inhale:",
    ":kannakms:",
    "love you too",
    "uwu",
    "on god?",
    "ok enough",
    "when will you stop talking",
    "why aren't you banned yet",
    "can we ban this guy",
    "please direct all complaints to brock",
    "MMMMMMMMMMMMM",
    "can you stop",
    "<:3",
    "ENOUGH BITCH",
    "YOU THINK THIS IS A GAME?",
    "ok im hacking you now",
    "hi",    
    "WHAT",    
    "sorry",  
    "kiss me",    
    ":homieskissing:",
    "nice anime pfp",
    "are you ok",
    "shower first",    
    "aha... yeah...",
    "can you stop pinging me",
    "WHY",
    "why",
    "no",
    "...nah",    
    "wtf",
    "bro",        
    ":boogaooga:",    
    "bruh",                
    "uh yeah ok",
    "you good?",
    "aiight",    
    "aiight g2g",
    "silence",
    "SILENCE",
    "i will proceed to step on you now",
    "bye",    
    "filth",
    "i will kill you",
    "why dont you talk to someone else",
    ":flushed:",
    "YOU IDIOT",
]

const specialResponses ={
    "420" : [":dance:"],
    "dance" : [":dance:", ":dansawa:", ":fastdance:", ":hyperboner:", ":adabzawa:", ":wigglin:"],
    "cajji" : [":cajjiw:"],    
    "ily" : ["always", "love you too", ":homieskissing:", "bitch i know it", "kiss me", "prove it", "hugzawa", ":flushed:"],
    "nom" : [":monchawa:", ":sennom:",":inhale:"],
    "yes or no" : ["yes", "no", "maybe", "sure", "nah", "...ok", "...nah", "definitely", "hell no", "perhaps", "yes... unless?", "no... unless?", "YES", "NO", "MAYBE", "let me think about it"],
    "mbl" :["MMMMMMMMMMMMMMMMMMM", "MMMMMM", "MMMMMMMMMM", "M", "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM", "MMMMMMMMMMM"],
}

const generalId = "518095108269408283";

methods ={
    initialize(client, guild){
        pack = this;
        pack.client = client;
        pack.guild = guild;

        console.log("Insults pack initialized.");        
    },    

    processCommand(msg, command, params){

    },

    processMessage(msg){
        if (msg.channel.id === generalId) return false;
        var mention = msg.mentions.users.last();        
        if (mention && mention.id == pack.client.user.id){
            this.respond(msg);
            return true;
        }
    },

    respond(msg){
        var index = tools.randomInt(0, responses.length);
        var string = responses[index];
        
        var response = this.getSpecialResponse(msg.content);
        if (response) string = response;

        var emoji = this.getEmoji(string);
        if (emoji) string = `${emoji}`;                                            
        
        string = tools.formatUserIdPingable(msg.author.id) + " " + string;
        tools.sendMessage(msg, string);
    },

    getEmoji(string){
        if (string[0] === ':' && string[string.length-1] === ':'){
            string = string.replace(/:/g,"");            
            var emoji = pack.client.emojis.find(emoji => emoji.name === string);
            if (emoji) return emoji;
        }
        return false;
    },

    getSpecialResponse(string){        
        string = string.replace("<@"+pack.client.user.id+">", "").trim();

        if (string.toLowerCase().includes("yes or no"))
            string = "yes or no";

        var response = specialResponses[string];
        if (response){
            var index = tools.randomInt(0, response.length);
            return response[index];
        }
        
        return false;
    }
}

module.exports = methods