const tools = require('./../../tools.js');

const adminId = '482300197070831627';
const modId = '519297711309193218';
const hmId = '542811191077109771';

var pack;

methods = {

    initialize(client, guild){
        pack = this;
        pack.client = client;
        pack.guild = guild;        
        
        console.log("Admin pack initialized.")
    },

    processCommand(msg, command, params){
        switch(command){
            case("say"):{
                pack.say(msg, params);
                return true;                
            }
        }
    },    

    say(msg, params){              

        if (!msg.member.roles.has(adminId) && !msg.member.roles.has(modId) && !msg.member.roles.has(hmId)) return;        
        if (params === undefined) return;

        var string = params.join(' ');
        tools.sendMessage(msg, string);
        msg.delete();
    },
}

module.exports = methods;