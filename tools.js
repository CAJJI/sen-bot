methods = {    
    
    // - ROOM TOOLS -

    //Sends a message in response to an incoming message. Optional box text formatting.
    sendMessage: function (msg, text, boxFormat, bold) {
        
        if (bold)
            text = this.bold(text);
        if (boxFormat)            
            text = this.boxFormat(text);        
        if (text){            
            msg.channel.send(text);
        }
    },     

    // - TEXT FORMATTING - 

    //Adds a line break to the end of text with an optional amount of lines.
    newLine(lineCount){
        var string = ""
        if (lineCount === undefined)
            string += '\n'
        for (line = 0; line < lineCount; line++)
            string += '\n';
        return string;
    },

    //Wraps text in a box format.
    boxFormat(string){        
        return "```" + this.newLine(1) + string + "```";
    },        

    bold(string){
        return "**" + string + "**";
    },

    // - MEMBER TOOLS - 

    //Takes a user id ("116745468100214793") and returns the user tag (name#0000).
    getUserTagFromId(guild, id){        
        var user = guild.members.get(id).user;
        return user.username + "#" + user.discriminator;
    },

    //Takes a user tag (name#0000) and returns a user.
    getUserFromTag(guild, tag){
        var split = tag.split('#');
        if (split.length <= 1) return;

        var username = split[0];
        var discriminator = split[1];

        guild.members.forEach(function(element){
            var user = element.user;
            if (user.username === username && user.discriminator === discriminator){                
                return user;
            }
        })
    },

    //Takes an array of user tags (name#0000) and returns an array of users.
    getUsersFromTags(guild, tags){
        if (tags.length <= 0) return [];

        var usernames = [];
        var discriminators = [];

        for (var i = 0 ; i < tags.length; i++){
            var split = tags[i].split('#');
            usernames.push(split[0]);
            discriminators.push(split[1]);
        }
        
        var users = [];        
        guild.members.forEach(function(element){
            var user = element.user;            
            for (var i = 0 ; i < usernames.length; i++){
                if (user.username === usernames[i] && user.discriminator === discriminators[i]){                    
                    console.log("found user " + user.username);                    
                    users.push(user);
                    usernames.splice(i);
                    discriminators.splice(i);
                    break;
                }
            }
            if (usernames.length <= 0)
            return users;
        })                
        return users;
    },

    //Takes a user tag (name#0000) and returns their id ("116745468100214793"). Option to wrap id in ping format.
    getUserIdFromTag(guild, tag, pingFormat){        
        var split = tag.split('#');
        if (split.length <= 1) return;

        var username = split[0];
        var discriminator = split[1];

        guild.members.forEach(function(element){
            var user = element.user;
            if (user.username === username && user.discriminator === discriminator){
                if (pingFormat)
                    return this.formatUserIdPingable(user.id);
                else
                    return user.id;
            }
        })
    },

    //Takes an array of user tags (name#0000) and returns an array of their ids ("116745468100214793"). Option to wrap ids in ping format.
    getUserIdsFromTags(guild, tags, pingFormat){        
        if (tags.length <= 0) return [];

        var usernames = [];
        var discriminators = [];

        for (var i = 0 ; i < tags.length; i++){
            var split = tags[i].split('#');
            usernames.push(split[0]);
            discriminators.push(split[1]);
        }
        
        var ids = [];        
        guild.members.forEach(function(element){
            var user = element.user;            
            for (var i = 0 ; i < usernames.length; i++){
                if (user.username === usernames[i] && user.discriminator === discriminators[i]){                                        
                    if (pingFormat)
                        ids.push(this.formatUserIdPingable(user.id));
                    else
                        ids.push(user.id);
                    usernames.splice(i);
                    discriminators.splice(i);
                    break;
                }
            }
            if (usernames.length <= 0)
            return ids;
        })                
        return ids;
    },

    //Formats a user id in a pingable format.
    formatUserIdPingable(id){
        return "<@" + id + ">";
    },  

    //Connects an array of strings with ", " with the final separator being "and". Optional box formatting.
    connectUsernamesString(users, boxFormat){
        var output = "";

        if (!Array.isArray(users) || users.length === 1){
            if (boxFormat)
                output += "`" + users + "`";
            else
                output += users;
            return output;
        }
                
        for (var i = 0 ; i < users.length; i++){
            if (i === users.length -1) output += " and ";
            
            if (boxFormat)
                output += "`" + users[i] + "`";
            else
                output += users[i];

            if (i < users.length -2) output += ", ";
        }
        return output;
    },

    // - MISCELLANEOUS - 

    //Returns a random int between min and max. NOT inclusive of max.
    randomInt (min, max) {
        return min + Math.floor(Math.random() * Math.floor(max));
    },

    //Returns a random int between 1 and 6.
    rollDice(){
        return this.randomInt(1,6);
    },
    
    //Returns any object as a string.
    toString(any){
        return any+='';
    },

    //Removes null values from an array.
    reEvaluateArray(array){
        var current = 0;
        var newArray = [];
        for (var a = 0; a < array.length; a++){
            if (array[a]){
                newArray[current] = array[a];
                current++;
            }
        }
        return newArray;
    },    
}

module.exports = methods