const tools = require('./../../tools.js');
const data = require('./data.json');

const fs = require('fs');

const maxHealth = 100;
const respawnTime = 1000 * 60 * 5;
const cooldown = 1000 * 60 * 3;

const expRewardOnWin = 50;

var pack;

//battle @____
//simulates a full battle and outputs the message
//save data to json (win/loss?)

//@____ action @_____'s limb (but @____ countered it) for __ (critical) damage!

const medActions =[
    "kicked",
    "punched",    
    "kneed",
    "elbowed",
    "gripped",    
    "clawed",    
    "stepped on",
    "roundhouse kicked",
    "right hooked",
    "left hooked",    
    "headbutt",    
    "struck",    
]

const weakActions =[
    "slapped",
    "smacked",
    "scratched",        
    "grazed",
    "bumped",    
    "jabbed",
    "backhanded",
    "bit",    
    "pulled",
    "twisted",    
]

const strongActions =[
    "crushed",
    "shattered",
    "smashed",
    "gouged",
    "dropkicked",
    "ripped out",    
    "throttled",
    "clobbered",
    "hammer fisted",
    "chamber punched",    
    "stomped",
    "destroyed",
    "thrashed",
    "obliterated",

]

const limbs = [
    "eye",
    "leg",
    "arm",
    "skull",
    "pelvis",
    "shoulder",
    "groin",
    "foot",    
    "nose",
    "hip",
    "knee",
    "mouth",
    "ear",
    "elbow",
    "knee",
    "shin",   
    "spine",
    "head",
    "chest",
    "ribs",

]

methods = {

    initialize(client, guild){
        pack = this;
        pack.client = client;
        pack.guild = guild;        

        console.log("Battle pack initialized.");                

        //test exp formulas
        // for (var i = 1; i < 10; i++){
        //     100 * i + 
        // }
    },

    processCommand(msg, command, params){
        switch(command){
            case("battle"):{
                this.battle(msg);
                return true;
            }
            case("slap"):{                
                this.attack(msg);                
                return true;                
            }
            case("disable"):{
                this.disable(msg);
                return true;                
            }
            case("enable"):{
                this.enable(msg);
                return true;                
            }
            case("refresh"):{                                                
                this.refresh(msg);                                                                                
                return true;                
            }      
            case("score"):{                                                
                this.getScore(msg);                                                                                
                return true;                
            }                  
            case("scoreboard"):{
                this.getHighscore(msg);
                return true;
            }
            case("reset"):{
                this.reset(msg, true);
                return true;
            }     
        }
    },

    initiateAttack(msg){
        var victim = msg.mentions.users.last();
        if (!victim) {
            tools.sendMessage(msg, "Please specify a target.", false, true);
            return;
        }   
        
        var attacker = msg.member.user;
                
        this.createUserIfNone(attacker);
        this.createUserIfNone(victim);                                    

        if (!data[attacker.id].enabled){
            tools.sendMessage(msg, attacker.username + ' you do not have battles enabled. To enable battles type "-enable"."', false, true)
            return;
        }

        if (!data[victim.id].enabled){
            tools.sendMessage(msg, victim.username + " does not have battles enabled.", false, true)
            return;
        }

        return {
            attacker : data[attacker.id],
            victim : data[victim.id],            
        }
    },

    battle(msg){
        var attack = this.initiateAttack(msg);
        if (!attack) return;

        if (attack.attacker == attack.victim){
            tools.sendMessage(msg, attack.attacker.username + " you cannot battle yourself!", false, true);
            return;
        }
        
        if (this.isKnockedOut(attack.attacker)){ 
            tools.sendMessage(msg, attack.attacker.username + " you are currently knocked out!", false, true)
            return;
        }

        if (this.isKnockedOut(attack.victim)){ 
            tools.sendMessage(msg, attack.victim.username + " is currently knocked out!", false, true)
            return;
        }

        this.simulateBattle(msg, attack.attacker, attack.victim);        
    },

    simulateBattle(msg, attackerData, victimData){        
        var string = "";
        var winner;
        var loser;

        while (attackerData.health > 0 && victimData.health > 0){

            var attacker;
            var victim;

            var attackerChance = tools.randomInt(1,11);
            if (attackerChance > 5){attacker = attackerData; victim = victimData;}
            else{attacker = victimData; victim = attackerData;}

            var damage = tools.randomInt(5, (maxHealth + 1)/2);

            var critical = false;
            critChance = tools.randomInt(1,11);
            if (critChance  > 7) critical = true;

            var counterChance = tools.randomInt(1,11);
            //var countered = counterChance > 5;        
            countered = false;

            var damageString = "damage!";

            var actions = medActions;  
            if (!critical){
                if (damage < maxHealth/10) actions = weakActions;
            }
            else{                                
                damage += maxHealth/2;                
                actions = strongActions;
                damageString = "critical damage!!"            
            }

            var actionIndex = tools.randomInt(0, actions.length);
            var limbIndex = tools.randomInt(0, limbs.length);            

            if (!countered){
                victim.health -= damage;                                            
                
                string += ">" + attacker.username + " " + actions[actionIndex] + " " + victim.username + "'s" + " " + limbs[limbIndex] 
                + " for " + damage + " " + damageString;
                // string += ">" + tools.bold(attacker.username) + " " + actions[actionIndex] + " " + tools.bold(victim.username + "'s") + " " + limbs[limbIndex] 
                // + " for " + tools.bold(damage) + " " + damageString;

                if (victim.health <= 0){ 
                    winner = attacker;
                    loser = victim;
                }
            }
            else{                        
                attacker.health -= damage;    
                
                string += ">"+ attacker.username + " " + actions[actionIndex] + " " + victim.username + "'s" + " " + limbs[limbIndex] 
                + ", but " + victim.username + " reversed it for " + damage + " " + damageString;
                // string += ">"+ tools.bold(attacker.username) + " " + actions[actionIndex] + " " + tools.bold(victim.username + "'s") + " " + limbs[limbIndex] 
                // + " but " + tools.bold(victim.username) + " reversed it for " + tools.bold(damage) + " " + damageString;
                
                if (attacker.health <= 0){ 
                    winner = victim;
                    loser = attacker;
                }
            }

            string += tools.newLine(2);
        }
        
        tools.sendMessage(msg, string, true);
        //tools.sendMessage(msg, string);

        tools.sendMessage(msg, tools.bold(winner.username) + " has won with " + tools.bold(winner.health + "/" + maxHealth) + " health remaining!!");

        this.knockOut(msg, loser);
        this.rewardExp(msg, winner, expRewardOnWin);
        winner.wins++;
        loser.defeats++;

        attackerData.battles++;
        victimData.battles++;
        this.save();
    },

    reset(msg, log){
        if (msg != null && !msg.member.hasPermission('ADMINISTRATOR')){
            tools.sendMessage(msg, "You do not have permission to do this!", false, true);
            return;
        }

        var dataSet = Object.values(data);        
        for (var i = 0; i < dataSet.length; i++){
            data[dataSet[i].id].cooldown = 0;
        }        
        this.save();
                
        if (log)
            tools.sendMessage(msg, "Cooldowns reset.");
    },

    getHighscore(msg){        
        var dataSet = this.sort();

        var string = "**__Slap Leaderboard__**"        
        
        for (var i = 0 ; i < 10; i++){
            if (i >= dataSet.length) break;
            string += tools.newLine();
            string += "> " + (i + 1) + ". " + tools.bold(dataSet[i].username) + " - " + tools.bold(dataSet[i].wins) + " W : " + tools.bold(dataSet[i].defeats) + " L";
        }

        tools.sendMessage(msg, string);
    },

    sort(){
        var dataSet = Object.values(data);
        dataSet.sort(this.compare); 
        return dataSet;        
    },

    compare(a, b){
        if (a.wins === b.wins){
            if (a.defeats === b.defeats) return 0;
            if (a.defeats < b.defeats) return -1;
            if (a.defeats > b.defeats) return 1;
        }

        if (a.wins < b.wins) return 1;
        if (a.wins > b.wins) return -1;
    },

    getScore(msg){
        var userData = this.createUserIfNone(msg.member.user);

        var mention = msg.mentions.users.last();
        if (mention) userData = this.createUserIfNone(mention.id);

        var dataSet = this.sort();        
        var rank = dataSet.indexOf(userData) + 1;

        tools.sendMessage(msg, tools.bold(userData.username) + " your current rank is " + tools.bold(rank) + " with " + tools.bold(userData.wins) + " wins, " + tools.bold(userData.defeats) 
        + " losses, and " + tools.bold(userData.slaps) + " slaps given.");
    },

    refresh(msg){                
        var user = msg.mentions.users.last();
        var admin = false;
        if (user){
            if (!msg.member.hasPermission('ADMINISTRATOR')){
                tools.sendMessage(msg, "You do not have permission to do this.", false, true);
                return;
            }
            admin = true;
            this.createUserIfNone(user);                        
        }
        else{
            user = msg.member.user;
        }

        var userData = this.createUserIfNone(user);

        if (this.isOnCoolDown(userData) && !admin){            
            tools.sendMessage(msg, "You can only use refresh once every 3 minutes!", false, true);
            return;
        }
        
        userData.health = maxHealth;
        userData.respawn = 0;
                
        if (!admin)
            this.applyCoolDown(userData);

        this.save();
        tools.sendMessage(msg, user.username + " has been refreshed for battle!", false, true);        
    },

    disable(msg){
        var user = msg.member.user;
        this.createUserIfNone(user);
        data[user.id].enabled = false;
        tools.sendMessage(msg, user.username + ' you have disabled battles. To enable battles type "-enable".', false, true)
        this.save();
    },

    enable(msg){
        var user = msg.member.user;
        this.createUserIfNone(user);
        data[user.id].enabled = true;
        tools.sendMessage(msg, user.username + ' you have enabled battles. To disable battles type "-disable".', false, true)
        this.save();
    },

    attack(msg){        
        var attack = this.initiateAttack(msg);
        if (!attack) return;

        if (attack.attacker == attack.victim){
            tools.sendMessage(msg, attack.attacker.username + " you cannot slap yourself!", false, true);
            return
        }

        if (this.isKnockedOut(attack.attacker)){ 
            tools.sendMessage(msg, attack.attacker.username + " you are currently knocked out!", false, true)
            return;
        }
        if (this.isKnockedOut(attack.victim)){ 
            tools.sendMessage(msg, attack.victim.username + " is currently knocked out!", false, true)
            return;
        }

        this.attackUser(msg, attack.attacker, attack.victim);
    },

    isOnCoolDown(userData){        
        var now = new Date();        
        if (userData.cooldown == 0 || now > Date.parse(userData.cooldown)){
            userData.cooldown = 0;
            return false;
        }
        return true;        
    },

    applyCoolDown(userData){
        var cooldownDate = new Date();
        cooldownDate.setTime(cooldownDate.getTime() + cooldown);

        userData.cooldown = cooldownDate;        
        this.save();
    },

    attackUser(msg, attacker, victim){       
        var damage = tools.randomInt(0, maxHealth + 1);

        var counterChance = tools.randomInt(1,11);
        var countered = counterChance > 5;                

        var string = "";

        if (damage === 0){
            tools.sendMessage(msg, tools.bold(attacker.username) + " you missed! Idiot! Nobody took damage.");
        }
        else if (!countered){
            victim.health -= damage;            
            if (damage > maxHealth/2){
                string = tools.bold(attacker.username) + " slapped for " + tools.bold(damage) + " critical damage!!";
            }
            else{
                string = tools.bold(attacker.username) + " slapped for " + tools.bold(damage) + " damage!";                
            }
            string += tools.newLine()
            + tools.bold(victim.username) + " has " + tools.bold(victim.health + "/" + maxHealth) + " health left.";

            tools.sendMessage(msg, string);

             if (victim.health <= 0){ 
                this.knockOut(msg, victim);
                this.rewardExp(msg, attacker, expRewardOnWin);
                attacker.wins++;
                victim.defeats++;
             }
        }
        else{                        
            attacker.health -= damage;            
            if (damage > maxHealth/2){
                string = tools.bold(victim.username) + " countered for " + tools.bold(damage) + " critical damage!! ";
            }
            else{
                string = tools.bold(victim.username) + " countered for " + tools.bold(damage) + " damage! ";            
            }

            string += tools.newLine()
            + tools.bold(attacker.username) + " has " + tools.bold(attacker.health + "/" + maxHealth) + " health left.";

            tools.sendMessage(msg, string);

             if (attacker.health <= 0){ 
                this.knockOut(msg, attacker);
                this.rewardExp(msg, victim, expRewardOnWin);
                victim.wins++;
                attacker.defeats++;
             }
        }

        attacker.slaps++;
        this.save();
    },

    rewardExp(msg, userData, exp){
        if (userData.lvl == null) userData.lvl = 1;
        var nextLevel = userData.lvl * 100;
        if (userData.exp == null) userData.exp = 0;
        userData.exp += exp;

        while (userData.exp >= nextLevel){
            userData.exp -= nextLevel;
            userData.lvl++;
            tools.sendMessage(msg, tools.bold(userData.username) + " has advanced to level " + tools.bold(userData.lvl) + "!");
            nextLevel = userData.lvl * 100;            
        }
        this.save();        
    },

    isKnockedOut(userData){
        if (userData.health > 0) return false;

        var now = new Date();        

        if (now > Date.parse(userData.respawn) || userData.respawn == 0){
            this.revive(userData);
            return false;
        }
        return true;
    },

    revive(userData){
        userData.health = maxHealth;
        userData.respawn = 0;
        this.save();
    },

    knockOut(msg, userData){
        userData.health = 0;

        var respawnDate = new Date();
        respawnDate.setTime(respawnDate.getTime() + respawnTime);

        userData.respawn = respawnDate;
        
        tools.sendMessage(msg, userData.username + ' has been knocked out for 5 minutes! To disable battles, type "-disable"', false, true);                
    },

    createUserIfNone(user){
        if (!data[user.id]){
            this.createUser(user);
        }
        return data[user.id];
    },

    createUser(user){        
        data[user.id] = {
            id: user.id,
            username: user.username,
            health: maxHealth,
            respawn: 0,   
            cooldown: 0,  
            enabled: true,  
            wins: 0,
            defeats: 0,
            slaps: 0,   
            battles: 0,  
            lvl : 1,
            exp: 0,
        }
    },

    save(){                 
        if (data){  
            fs.writeFile('./packs/battle/data.json', JSON.stringify(data, null, 4), err => {                
                if (err) throw err;
            });   
        }
        else
            console.log("corrupt user data.");
    },
}

module.exports = methods;