const tools = require('./../../tools.js');
const google = require('./google.js');


//Set up for Sen's server
const bdayRole = "597845563567898663";
const generalChannel = "518095108269408283";

var pack;

var cachedResponses;

methods = {    

    initialize : function(client, guild){                        
        pack = this;
        pack.client = client;
        pack.guild = guild;
        pack.revokeRolesFrom = [];

        var today = new Date();
        var tomorrow = new Date()
        tomorrow.setDate(today.getDate() + 1);
        tomorrow.setHours(4,0,0,0);                       

        var timeUntilMidnight = tomorrow - today;

        pack.processBirthdays(client);

        setTimeout(pack.initializeAutomation, timeUntilMidnight);    

        console.log("Birthday pack initialized.")
    },    

    initializeAutomation : function(client){        
        pack.processBirthdays(client);

        var interval = 1000 * 60 * 60 * 24;        
        setInterval(pack.processBirthdays, interval);
    },    

    processBirthdays : function(){        
        google.callFunction('getResponses', function(response){          
            cachedResponses = response;        
            pack.processResponses(cachedResponses);            
            pack.revokeCurrentBirthdays(pack);     
            pack.setCurrentBirthdays(pack);                                                                   
            });
    },

    processResponses : function(responses){
        var today = new Date();
        for (var i = 0 ; i < responses.length; i++){
            var birthdate = responses[i].birthdate;

            var date = new Date(birthdate);
            date.setFullYear(today.getFullYear());
            date.setHours(4,0,0,0);

            responses[i].birthdate = date;            
        }
        responses.sort(pack.compare);                
    },
    
    revokeCurrentBirthdays(pack){        
        var guild = pack.guild;        
        var role = guild.roles.get(bdayRole);   

        var today = pack.getToday();
        var birthdaysToday = pack.getBirthdays(today);                                

        role.members.forEach(function(element){                
            var skip = false;
            for (var i = 0 ; i < birthdaysToday.length; i++){
                if (birthdaysToday[i].tag == tools.getUserTagFromId(guild, element.user.id)){                    
                    skip = true;
                }
            }
            if (!skip) {
                console.log("revoking birthday role from " + element.user.username);
                element.removeRole(role);            
            }
        });              
    },    

    setCurrentBirthdays(pack){
        var guild = pack.guild;
        var role = guild.roles.get(bdayRole);

        var today = pack.getToday();
        var birthdaysToday = pack.getBirthdays(today);                

        if (birthdaysToday.length > 0){
            console.log("Today's birthdays are :");
            console.log(birthdaysToday);   
        }     
        
        var ids = tools.getUserIdsFromTags(guild, pack.getTags(birthdaysToday));        
        if (ids.length <= 0) return;        
                
        var pingMemberUsernames = [];
        for (var i = 0 ; i < ids.length; i++){            

            var member = guild.members.get(ids[i]);
            if (!member.roles.has(bdayRole)){
                member.addRole(role).catch(console.error);
                pack.revokeRolesFrom.push(ids[i]);
                console.log("giving birthday role to "+ ids[i]);            
                pingMemberUsernames.push(tools.formatUserIdPingable(ids[i]));
            }
        }        

        if (pingMemberUsernames.length <= 0) return;
        guild.channels.get(generalChannel).send("**HAPPY BIRTHDAY " + tools.connectUsernamesString(pingMemberUsernames) + " !!!**");
    },   

    processCommand(msg, command, params){
        switch(command){
            case("bday") : {
                if (params.length > 0){
                    if (params[0] == "revoke") pack.revokeAllBirthdays(msg);
                    if (params[0] == "refresh") pack.refreshBirthdays(msg);
                }
                else
                    pack.nextBirthday(msg);                                                                    
                return true;            
            }            
        }        
    }, 
    
    nextBirthday(msg){                  
        //var split = cachedResponses[0].birthdate.toUTCString().split(' ');
        //var nextBirthdate = split[2] + " " + split[1];

        var today = pack.getToday();
        var birthdaysToday = pack.getBirthdays(today);                

        var string = "";

        if (birthdaysToday.length === 1){
            string += "**It's ``" + birthdaysToday[0].tag + "``'s birthday today!!**"
                + tools.newLine(2);            
        }

        else if (birthdaysToday.length > 1){
            var members = [];
            for (var i = 0 ; i < birthdaysToday.length; i++){                                    
                members.push(birthdaysToday[i].tag);                                   
            }            
            var connectedMembersString = tools.connectUsernamesString(members, true);
            string += "**Today's birthday boys are " + connectedMembersString + "!!**"
                + tools.newLine(2);            
        }

        string += "**__UPCOMING BIRTHDAYS__**\n"

        var upcomingBirthdays = [];
        var index = -1;
        while (upcomingBirthdays.length < 5){
            index++;
            if(birthdaysToday.includes(cachedResponses[index])){                                
                continue;
            } 
            upcomingBirthdays.push(cachedResponses[index]);            
        }

        for (var i = 0 ; i < upcomingBirthdays.length; i++){
            var split = upcomingBirthdays[i].birthdate.toUTCString().split(' ');
            var birthdate = split[2] + " " + split[1];
            string += (i+1) + ". " + tools.bold(upcomingBirthdays[i].tag) + " - " + birthdate + "\n";
        }

        console.log(string);
        tools.sendMessage(msg, string);

        //var upcomingBirthdays = pack.getBirthdays(cachedResponses[0].birthdate);

        // var tags = tools.connectUsernamesString(pack.getTags(upcomingBirthdays), true);                
        
        // if (upcomingBirthdays.length > 1)
        //     tools.sendMessage(msg, "The next birthdays are on **" + nextBirthdate + "** for " + tags + "!");        
        // else
        //     tools.sendMessage(msg, "The next birthday is on **" + nextBirthdate + "** for " + tags + "!");        
    },    

    revokeAllBirthdays(msg){        
        if (!msg.member.hasPermission('ADMINISTRATOR')) return;

        var guild = pack.guild;        
        var role = guild.roles.get(bdayRole);   
        
        role.members.forEach(function(element){                            
            console.log("revoking birthday role from " + element.user.username);
            element.removeRole(role);                    
            tools.sendMessage(msg, "All birthday roles revoked.");
        });              
    },    

    refreshBirthdays(msg){
        if (!msg.member.hasPermission('ADMINISTRATOR')) return;
        
        pack.processBirthdays();
        tools.sendMessage(msg, "Birthdays processed.");
    },

    getBirthdays(date){        
        var birthdays = [];        
        for (var i = 0; i < cachedResponses.length; i++){
            if (pack.isSameDate(cachedResponses[i].birthdate, date)){
                birthdays.push(cachedResponses[i])                                
            }
        }        
        return birthdays;
    },

    getToday(){
        var date = new Date();
        date.setHours(4,0,0,0);
        return date;
    },

    getTags(responses){
        var tags = [];
        for (var i = 0; i < responses.length; i++){
            tags.push(responses[i].tag);
        }
        return tags;
    },

    compare(a, b){
        var today = pack.getToday();   

        //today = today.toString();
        var first = a.birthdate;
        var second = b.birthdate;
                
        if (pack.isSameDate(today, a.birthdate)){ 
            return -1;
        }
        if (first - today < 0) return 1;
        if (second - today < 0) return -1;
        if (first - today < second - today) return -1;
        if (first - today > second - today) return 1;
        return 0;              
    },

    isSameDate(a, b){
        return a.getDate() === b.getDate() && a.getMonth() === b.getMonth() && a.getFullYear() === b.getFullYear();
    }
}

module.exports = methods