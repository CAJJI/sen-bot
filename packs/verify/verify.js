const fs = require('fs');

const config = require("./config.json");

const adminId = '482300197070831627';
const testId = '609689897783132171';

var pack;

methods = {    

    initialize(client, guild){                
        pack = this;
        pack.client = client;
        pack.guild = guild;
        pack.enabled = true;

        console.log("Verification pack initialized.")
    },

    processCommand(msg, command, params){
        switch(command){
            case("verify"):{
                if(msg.channel.id == config.channelID){
                    try{
                        msg.member.addRole(config.roleID);
                    }
                    catch(err){
                        console.log("Unable to give verification role");
                    }
                }
                return;
            }
            case("roleID"):{
                if (!msg.member.roles.has(adminId)) return; 
                try{
                    newRole = this.guild.roles.get(params[0])
                    config.roleID = newRole.id;
                    msg.channel.send("Changing verify role to "+newRole.name);
                    configData = JSON.stringify(config, null, 4);
                    fs.writeFile('./packs/verify/config.json', configData);
                }
                catch(err){
                    console.log(err);
                    msg.channel.send("Couldn't find role from ID");
                }
                return;
            }
            case("channelID"):{
                if (!msg.member.roles.has(adminId)) return; 
                try{
                    newChannel = this.guild.channels.get(params[0]);
                    config.channelID = newChannel.id;
                    msg.channel.send("Changing verify channel to #"+newChannel.name);
                    configData = JSON.stringify(config, null, 4);
                    fs.writeFile('./packs/verify/config.json', configData);
                }
                catch(err){
                    console.log(err);
                    msg.channel.send("Couldn't find role from ID");
                }
                return;
            }
        }
    }

}

module.exports = methods